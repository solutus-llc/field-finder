import { test, expect, devices } from '@playwright/test';
import axios from 'axios';
import fs from 'fs';
import path from 'path';
import { google } from 'googleapis';
import { JSDOM } from 'jsdom';

test.use({
    ...devices['Desktop Chrome'],
    headless: false, // Override the headless option for this specific test
});

async function solveCaptcha(page) {
    const CAPSOLVER_API_KEY = 'CAP-77D4EF3816B0FFB899EE1A9C15F9BB8F';
    const websiteURL = 'https://shisetsu-yoyaku.jp/ajisai/Welcome.cgi'; // 対象のURL

    // ページから siteKey を動的に取得
    const siteKey = await page.evaluate(() => {
        const widget = document.querySelector('capsolver-widget[data-captcha-type]');
        if (widget) {
            return widget.getAttribute('data-sitekey');
        }
        return '6Lckh8UeAAAAAKVkPyyn8SjebFaCJun2GjeZ5v4p';
    });

    if (!siteKey) {
        throw new Error('siteKeyの取得に失敗しました');
    }

    const taskPayload = {
        type: 'ReCaptchaV2TaskProxyless',
        websiteURL: websiteURL,
        websiteKey: siteKey,
    };

    // taskPayload をログに出力
    console.log('taskPayload:', taskPayload);

    async function createTask(payload) {
        try {
            const res = await axios.post('https://api.capsolver.com/createTask', {
                clientKey: CAPSOLVER_API_KEY,
                task: payload
            });
            return res.data;
        } catch (error) {
            console.error('Error creating task:', error);
            throw error;
        }
    }

    async function getTaskResult(taskId) {
        try {
            let success = false;
            while (!success) {
                await new Promise(resolve => setTimeout(resolve, 1000));
                console.log("Getting task result for task ID: " + taskId);
                const res = await axios.post('https://api.capsolver.com/getTaskResult', {
                    clientKey: CAPSOLVER_API_KEY,
                    taskId: taskId
                });
                if (res.data.status == "ready") {
                    success = true;
                    return res.data;
                }
            }
        } catch (error) {
            console.error('Error getting task result:', error);
            throw error;
        }
    }

    const taskData = await createTask(taskPayload);
    const response = await getTaskResult(taskData.taskId);

    if (response && response.solution) {
        return response.solution.gRecaptchaResponse; // 解決したトークンを返す
    }
    throw new Error('CAPTCHA解決に失敗しました');
}

test('ログインと予約照会', async ({ page, context }) => {
    test.setTimeout(150000);

    await context.clearPermissions();

    // Playwrightのpageオブジェクトを使用するため、chromium.launchは不要
    await page.goto('https://shisetsu-yoyaku.jp/ajisai/Welcome.cgi');

    // ID とパスワードの入力を待機
    await page.waitForSelector('input[name="txtRiyoshaCode"]');
    await page.fill('input[name="txtRiyoshaCode"]', '2009129');
    await page.waitForSelector('input[name="txtPassWord"]');
    await page.fill('input[name="txtPassWord"]', 'Uz86NDxgeaGfpyeD');

    // CAPTCHAを解決
    let captchaToken;
    try {
        captchaToken = await solveCaptcha(page);
    } catch (error) {
        console.error('CAPTCHA解決に失敗しました: ', error);
        return; // エラーが発生した場合は次へ進まない
    }

    // reCAPTCHA トークンを設定
    await page.evaluate(token => {
        document.querySelector('textarea[name="g-recaptcha-response"]').value = token;
    }, captchaToken);

    // パスワード欄から2回Tabキーを押してフォーカスを移動し、スペースキーを押してチェックボックスにチェックを入れる
    await page.keyboard.press('Tab');
    await page.keyboard.press('Tab');
    await page.keyboard.press('Space');

    // チェックが入るのを待機
    await page.waitForTimeout(3000);

    // すべてのデータが解決したらログインボタンをクリック
    await page.locator('p.loginbtn a').click(); // ログインボタンをクリック

    await page.waitForURL('https://shisetsu-yoyaku.jp/ajisai/menu/Login.cgi'); // Ensure the URL is correct

    // 自動操作開始
    try {
        // Try to click the specific link
        await page.getByRole('link', { name: '予約照会・取消 利用が確定した申込を確認・取消できます。' }).click({ timeout: 5000 });
    } catch (error) {
        console.log('Specific link not found, clicking any available link.');

        // If the specific link is not found, click the first available link on the page
        const links = await page.$$('a'); // Select all link elements
        if (links.length > 0) {
            await links[0].click(); // Click the first link
            await page.waitForLoadState('networkidle');
            await page.getByRole('link', { name: '予約照会・取消 利用が確定した申込を確認・取消できます。' }).click();
        } else {
            console.log('No links found on the page.');
        }
    }

    await page.waitForURL('https://shisetsu-yoyaku.jp/ajisai/menu/Menu.cgi');

    // 利用年月のオプション値を取得して配列に追加
    const dates = await page.evaluate(() => {
        const options = document.querySelectorAll('select[name="riyoShinseiYM"] option');
        return Array.from(options).map(option => option.value);
    });

    // 表示内容を一時的に保存するための配列
    const results = [];

    // 日付配列をループで回して順次「表示ボタン」をクリック
    for (const date of dates) {
        await page.locator('select[name="riyoShinseiYM"]').selectOption(date);
        await page.getByRole('link', { name: '表示ボタン' }).click();

        // ページロードを待機
        await page.waitForLoadState('load'); // Wait for the page to load completely

        // 表示内容を取得
        const content = await page.locator('table[summary="予約一覧検索結果"]').innerHTML(); // Get the inner HTML of the table
        results.push(content); // ここで content を results に追加

        // "次の30件" が存在する場合、クリックして再度 push
        while (await page.getByRole('link', { name: '次の30件' }).isVisible()) {
            await page.getByRole('link', { name: '次の30件' }).click();
            await page.waitForLoadState('load'); // Wait for the page to load completely
            const moreContent = await page.locator('table[summary="予約一覧検索結果"]').innerHTML();
            results.push(moreContent); // ここで moreContent を results に追加
        }
    }

    // tableLocatorを定義
    const tableLocator = page.locator('table[summary="予約一覧検索結果"]');

    // table内容を HTML としてファイルに保存
    const htmlContent = results.join('');
    fs.writeFileSync(path.join(__dirname, 'reservation_results.html'), htmlContent, 'utf8');

    // // Google Sheets API を使ってデータをスプレッドシートに書き込む
    // const authClient = await auth.getClient();
    // const sheets = google.sheets({ version: 'v4', auth: authClient });

    // const spreadsheetId = '1r5Ab8Xb6gTM6n5-7KmBttcoASu5YMaO8JzYy-R_fVNg'; // Google SpreadsheetのIDを設定
    // const range = 'Sheet1!A1'; // 書き込む範囲を指定

    // // クリップボードの内容をそのままスプレッドシートに書き込む
    // const clipboardContent = clipboardy.readSync();
    // const dom = new JSDOM(clipboardContent);
    // const document = dom.window.document;
    // const table = document.querySelector('table');
    // const rows = Array.from(table.querySelectorAll('tr')).map(row => {
    //     const cells = Array.from(row.querySelectorAll('th, td'));
    //     return cells.map(cell => cell.textContent.trim());
    // });

    // await sheets.spreadsheets.values.append({
    //     spreadsheetId,
    //     range,
    //     valueInputOption: 'RAW',
    //     requestBody: {
    //         values: dom,
    //     },
    // });

    console.log('データがスプレッドシートに追加されました。');

    // table 直下の tbody 要素を削除
    await tableLocator.evaluate((table) => {
        const tbody = table.querySelector('tbody');
        if (tbody) {
            tbody.remove(); // tbody 要素を削除
        }
    });

    // 取得した内容を table 直下に新しい tbody として追加
    await tableLocator.evaluate((table, results) => {
        const newTbody = document.createElement('tbody'); // 新しい tbody を作成
        newTbody.innerHTML = results.join(''); // results を tbody に追加
        table.appendChild(newTbody); // table 直下に新しい tbody を追加
    }, results);

    // 最初の td 要素に selectedYoyakuUniqKey を追加
    await tableLocator.evaluate((table) => {
        const rows = table.querySelectorAll('tbody tr'); // tbody内の全ての行を取得
        rows.forEach(row => {
            const onclickAttr = row.getAttribute('onclick'); // trのonclick属性を取得
            if (onclickAttr) { // onclickAttrがnullでないことを確認
                const match = onclickAttr.match(/oneClickSubmit\('selectedYoyakuUniqKey','Enter','(.*?)'\)/);
                if (match) {
                    const selectedYoyakuUniqKey = match[1]; // 抽出したキー
                    const firstTd = row.querySelector('td'); // 最初のtdを取得
                    if (firstTd) {
                        const uniqKeyDiv = document.createElement('div'); // div要素を作成
                        uniqKeyDiv.textContent = selectedYoyakuUniqKey; // divにキーを設定
                        firstTd.appendChild(uniqKeyDiv); // firstTdの直下にdivを追加
                    }
                }
            }
        });
    });

    // ワンクリックでコピーできるボタンを追加
    await tableLocator.evaluate((table) => {
        const copyButton = document.createElement('button'); // ボタンを作成
        copyButton.textContent = 'テーブルをコピー'; // ボタンのテキスト
        copyButton.style.marginBottom = '10px'; // ボタンのスタイル調整

        // ボタンのクリックイベントを設定
        copyButton.onclick = () => {
            const range = document.createRange();
            range.selectNodeContents(table); // テーブルの内容を選択
            const selection = window.getSelection();
            selection.removeAllRanges(); // 既存の選択範囲をクリア
            selection.addRange(range); // 新しい範囲を追加

            // クリップボードにコピー
            document.execCommand('copy');
        };

        table.parentNode.insertBefore(copyButton, table); // テーブルの前にボタンを追加

        // クリック操作を発生させる
        copyButton.click();
    });
});
