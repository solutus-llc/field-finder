const { devices } = require('@playwright/test');
const path = require('path');

module.exports = {
    workers: 3,
    fullyParallel: true,
    use: {
        headless: true,
        screenshot: 'on',
        trace: 'on',
    },
    // projects: [
    //     {
    //         name: 'chrome',
    //         use: { ...devices['Desktop Chrome'] },
    //     },
    // ],
};
