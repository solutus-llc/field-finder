import { test, expect } from '@playwright/test';
import fs from 'fs';
import path from 'path';
import looksSame from 'looks-same';
const axios = require('axios');
const FormData = require('form-data');

const testCases = [
    {
        metaRiyouMokuteki: '運動施設／体育館／王子・中央・磯上・垂水などの大型体育館',
        riyouMokuteki: 'バドミントン',
        gyms: ['中央体育館', '磯上体育館', '垂水体育館', '西体育館', '須磨体育館'],
    },
    {
        metaRiyouMokuteki: '運動施設／体育館／王子・中央・磯上・垂水などの大型体育館',
        riyouMokuteki: 'その他武道',
        gyms: ['垂水体育館'],
    },
    {
        metaRiyouMokuteki: '運動施設／体育室／文化センター・公民館・その他の運動施設',
        riyouMokuteki: 'バドミントン',
        gyms: [
            '葺合公民館',
            '東垂水公民館',
            '清風公民館',
            '長田公民館',
            '南須磨公民館',
            '灘区文化センター',
            '兵庫区文化センター',
            '長田区文化センター',
        ],
    }
];

async function sendTelegramDocument(imagePath) {
    const botToken = '5879222809:AAHekI-0F2lAxeyLuaRBPn8lv5BSy5VU2ZM';
    const chatId = '1809238421';
    const telegramApiUrl = `https://api.telegram.org/bot${botToken}/sendDocument`;

    const formData = new FormData();
    formData.append('chat_id', chatId);
    formData.append('document', fs.createReadStream(imagePath));
    formData.append('caption', `Image path: ${imagePath}`);

    try {
        const response = await axios.post(telegramApiUrl, formData, {
            headers: formData.getHeaders(),
        });
        console.log('Telegramにドキュメントを送信しました:', response.data);
    } catch (error) {
        console.error('Telegramへのドキュメント送信に失敗しました:', error);
    }
}

async function sendTelegramNotification(imagePath) {
    const botToken = '5879222809:AAHekI-0F2lAxeyLuaRBPn8lv5BSy5VU2ZM';
    const chatId = '1809238421';
    const telegramApiUrl = `https://api.telegram.org/bot${botToken}/sendPhoto`;

    const formData = new FormData();
    formData.append('chat_id', chatId);
    formData.append('photo', fs.createReadStream(imagePath));

    try {
        await axios.post(telegramApiUrl, formData, {
            headers: formData.getHeaders(),
        });
        console.log('Telegramに通知を送信しました。');
    } catch (error) {
        console.error('Telegramへの通知送信に失敗しました:', error);
    }
}

async function sendTelegramDocuments(imagePaths) {
    const botToken = '5879222809:AAHekI-0F2lAxeyLuaRBPn8lv5BSy5VU2ZM';
    const chatId = '1809238421';
    const telegramApiUrl = `https://api.telegram.org/bot${botToken}/sendMediaGroup`;

    const media = imagePaths.map((imagePath, index) => ({
        type: 'photo',
        media: `attach://${index}`,
        caption: `Image path: ${imagePath}`
    }));

    const formData = new FormData();
    formData.append('chat_id', chatId);
    formData.append('media', JSON.stringify(media));

    imagePaths.forEach((imagePath, index) => {
        formData.append(index.toString(), fs.createReadStream(imagePath));
    });

    try {
        const response = await axios.post(telegramApiUrl, formData, {
            headers: formData.getHeaders(),
        });
        console.log('Telegramに複数のドキュメントを送信しました:', response.data);
    } catch (error) {
        console.error('Telegramへの複数ドキュメント送信に失敗しました:', error);
    }
}

// 新しい関数を追加
async function sendDocumentToGroup(filePath, groupChatId, gyms, riyouMokuteki) {
    const botToken = '5879222809:AAHekI-0F2lAxeyLuaRBPn8lv5BSy5VU2ZM';
    const telegramApiUrl = `https://api.telegram.org/bot${botToken}/sendDocument`;

    const formData = new FormData();
    formData.append('chat_id', groupChatId);
    formData.append('document', fs.createReadStream(filePath));
    // Caption を gyms と riyouMokuteki を用いた書式に変更（複数の場合はカンマ区切り）
    formData.append('caption', `${gyms.join(", ")} - ${riyouMokuteki}`);

    try {
        const response = await axios.post(telegramApiUrl, formData, {
            headers: formData.getHeaders()
        });
        console.log('Document sent to Telegram group:', response.data);
    } catch (error) {
        console.error('Error sending document to Telegram group:', error);
    }
}

testCases.forEach(({ gyms, metaRiyouMokuteki, riyouMokuteki }) => {
    test(`${metaRiyouMokuteki}-${riyouMokuteki}`, async ({ page }) => {
        test.setTimeout(200000);

        await page.goto('https://shisetsu-yoyaku.jp/ajisai/Welcome.cgi');
        await page.getByRole('link', { name: '空き状況照会ボタン' }).click();
        await page.getByRole('link', { name: '詳細条件を指定して絞り込む' }).click();
        await page.locator('select[name="metaRiyouMokuteki"]').selectOption(metaRiyouMokuteki);
        await page.waitForLoadState('load');
        await page.locator('select[name="riyouMokuteki"]').selectOption(riyouMokuteki);
        await page.locator('#pagerbox').getByRole('link', { name: '次に進むボタン' }).click();

        await page.waitForLoadState('load');

        // "体育館全面"、"第２体育室（練習利用）"、"多目的室"、"体育室（練習利用）"を含むすべての<tr>要素を削除
        await page.$$eval('tr:has-text("体育館全面"), tr:has-text("第２体育室（練習利用）"), tr:has-text("多目的室"), tr:has-text("東灘体育館 体育室（練習利用）")', rows => {
            rows.forEach(row => row.remove());
        });

        for (const gym of gyms) {
            await page.getByRole('link', { name: `未選択 ${gym}` }).click();
            await page.waitForTimeout(200); // Add a 100ms wait time
        }

        await page.locator('#pagerbox').getByRole('link', { name: '次に進むボタン' }).click();

        // ページの完全な読み込みを待つ
        await page.waitForLoadState('load');

        // Create a directory for screenshots based on the test title
        const testTitle = `${metaRiyouMokuteki}-${riyouMokuteki}`;
        const dirPath = path.join(__dirname, 'screenshots', testTitle); // Store in screenshots directory
        if (!fs.existsSync(dirPath)) {
            fs.mkdirSync(dirPath, { recursive: true });
        }

        // Get the current Unix timestamp in milliseconds
        const currentDate = new Date();
        const jstOffset = 9 * 60; // 9 hours in minutes
        const jstDate = new Date(currentDate.getTime() + (jstOffset * 60000));
        const timestamp = jstDate.getTime(); // Unix timestamp in milliseconds

        // 1. 現在の年月日 = currentdate
        const dates = [
            jstDate,
            new Date(jstDate.getTime() + 31 * 24 * 60 * 60 * 1000),
            new Date(jstDate.getTime() + 62 * 24 * 60 * 60 * 1000)
        ];

        let combinedContent = '';

        for (const date of dates) {
            const formattedDate = date.toISOString().split('T')[0]; // YYYY-MM-DD形式に変換

            // 日付を選択
            const [year, month, day] = formattedDate.split('-');
            try {
                await Promise.all([
                    page.selectOption('#optYear', year, { timeout: 2000 }),
                    page.selectOption('#optMonth', month, { timeout: 2000 }),
                    page.selectOption('#optDay', day, { timeout: 2000 })
                ]);
            } catch (error) {
                console.error(`Date selection failed for ${formattedDate}:`, error);
                continue; // 日付選択に失敗した場合は次のループへ
            }

            // 指定された要素をクリック
            await page.click('#radioshowmonth');

            // 2. 表示ボタンを押してページ遷移の完了を待つ
            await page.getByRole('link', { name: '表示ボタン' }).click();
            await page.waitForLoadState('load');

            // 3. //*[@id="mmaincolumn"]/div/table/tbody を取得し、
            //    td 要素内の img 要素のうち、titleが "空いています" 以外のものを削除する
            const tbodyContent = await page.$eval('#mmaincolumn > div > table', table => {
                table.querySelectorAll('td').forEach(td => {
                    // Ensure td is a positioning context
                    if (getComputedStyle(td).position === 'static') {
                        td.style.position = 'relative';
                    }
                    td.querySelectorAll('img').forEach(img => {
                        if (img.getAttribute('title') !== '空いています') {
                            img.remove();
                        } else {
                            // Create a span with the title text and position it left0 bottom0
                            const span = document.createElement('span');
                            span.textContent = img.getAttribute('title'); // "空いています"
                            span.style.position = 'absolute';
                            span.style.left = '0';
                            span.style.bottom = '0';
                            span.style.whiteSpace = 'nowrap';
                            img.parentNode.insertBefore(span, img.nextSibling);
                        }
                    });
                });
                return table.outerHTML;
            });

            // 4. 次の date にループして1-3 を繰り返す
            combinedContent += `<div class="tablebox"><table class="facilitiesbox">${tbodyContent}</table></div>`;
        }

        // 5. 全ての month tbody を横並べに表示
        await page.evaluate((content) => {
            const container = document.querySelector('#mmaincolumn > div.tablebox');
            container.innerHTML = `<div style="display: flex;">${content}</div>`;
        }, combinedContent);

        await page.waitForTimeout(500);

        // ページの幅と高さを取得してビューポートを調整
        const { width, height } = await page.evaluate(() => {
            return {
                width: document.documentElement.scrollWidth,
                height: document.documentElement.scrollHeight
            };
        });
        await page.setViewportSize({ width: Math.floor(width * 1.7), height });

        // Save the screenshot with the Unix timestamp in the directory
        const screenshotPath = path.join(dirPath, `screenshot_${timestamp}.png`);
        await page.screenshot({ path: screenshotPath, fullPage: true });

        // Get the list of screenshots in the directory
        const files = fs.readdirSync(dirPath).filter(file => file.endsWith('.png')).sort();

        // Define the maximum number of screenshots to keep
        const maxScreenshots = 1440;

        // If the number of screenshots exceeds the maximum, delete the oldest ones
        if (files.length > maxScreenshots) {
            const filesToDelete = files.slice(0, files.length - maxScreenshots);
            for (const file of filesToDelete) {
                fs.unlinkSync(path.join(dirPath, file));
                console.log(`Deleted old screenshot: ${file}`);
            }
        }

        if (files.length > 1) {
            const latestScreenshot = path.join(dirPath, files[files.length - 1]);
            const previousScreenshot = path.join(dirPath, files[files.length - 2]);
            const { equal } = await looksSame(latestScreenshot, previousScreenshot, { createDiffImage: true });
            const diffImagePath = path.join(dirPath, `screenshot_${timestamp}.diff.png`);
            await looksSame.createDiff({
                reference: previousScreenshot,
                current: latestScreenshot,
                diff: diffImagePath,
                highlightColor: '#00FF00' // 補色（緑色）を指定
            });

            await sendTelegramDocument(diffImagePath);
            await sendTelegramDocument(latestScreenshot);

            if (!equal && process.env.NODE_ENV === 'prod') {
                await sendDocumentToGroup(diffImagePath, '-1002276207230', gyms, riyouMokuteki);
                await sendDocumentToGroup(latestScreenshot, '-1002276207230', gyms, riyouMokuteki);
                console.log('差分が見つかり、差分画像が作成されました。');
            } else {
                // Delete the latest screenshot if no differences are found
                fs.unlinkSync(latestScreenshot);
                console.log('差分が見つからなかったため、最新の画像を削除しました。');
            }
        }
    });
});
